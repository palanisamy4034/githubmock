import { Injectable } from '@angular/core';
import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { map, filter, catchError, mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GithubServiceService {
  public headers: Headers;
  private ipInfoUrl = '//iplocator.gofrugal.com/iplocator.php';
  api_url: string = 'https://zohoconnect.gofrugal.com/zoho/books/config/';
  constructor(private http: Http) {
    this.headers = new Headers();
    this.headers.append('Access-Control-Allow-Headers', 'Content-Type');
    this.headers.append('Access-Control-Allow-Methods', 'GET, POST');
    this.headers.append('Access-Control-Allow-Origin', '*');
    this.headers.append('Content-Type', 'application/json');
  }

  private handleError(error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body || {};
  }
  public GetRepoDetails(): Observable<any> {
    let _endPoint: string = "https://api.github.com/users/supreetsingh247/repos";
    return this.http
      .get(_endPoint)
      .pipe(map<any, any>((res) => this.extractData(res)),
        catchError<any, any>((res) => this.handleError(res)))
  }
  public GetUserInfo(): Observable<any> {
    let _endPoint: string = "https://api.github.com/users/supreetsingh247";
    return this.http
      .get(_endPoint)
      .pipe(map<any, any>((res) => this.extractData(res)),
        catchError<any, any>((res) => this.handleError(res)))
  }

}
