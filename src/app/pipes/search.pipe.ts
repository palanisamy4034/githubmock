import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(items: any[], value: string, lang?: string, repotype?: any): any[] {
    // if (!items) return [];

    if (items && items.length > 0) {
      return items.filter(item => {
        if (item.name != null && value !== undefined) {
          console.log(item.name, 'aaa');
          if (item.name.toLocaleLowerCase().indexOf(value.toLocaleLowerCase()) === -1) {
            return false;
          }
          if (item.language != null && lang !== 'All' && lang !== undefined) {
            // console.log(item.lang);
            if (item.language.toLowerCase().indexOf(lang.toLowerCase()) === -1) {
              return false;
            }
          } else {
            if (item.language == null && lang !== 'All') {
              return false;
            }
          }
          if (repotype !== 'All') {
            if (repotype !== 'public') {
              return item[repotype] === true ? true : false;
            }
          }

        }
        return true;
      });
    } else {
      return items;
    }

  }


}
