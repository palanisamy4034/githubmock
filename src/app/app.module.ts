import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ProfileComponent } from './components/profile/profile.component';
import { CollaborationComponent } from './components/collaboration/collaboration.component';
import { OverviewComponent } from './components/collaboration/overview/overview.component';
import { RepositoriesComponent } from './components/collaboration/repositories/repositories.component';
import { StarsComponent } from './components/collaboration/stars/stars.component';
import { FollowersComponent } from './components/collaboration/followers/followers.component';
import { FollowingComponent } from './components/collaboration/following/following.component';
import { GithubRoutingModule } from './github-routing.module';
import { ProfileInfoComponent } from './components/profile/profile-info/profile-info.component';
import { HttpModule } from '@angular/http';
import { SearchPipe } from './pipes/search.pipe';
import { FormsModule } from '@angular/forms'

@NgModule({
  declarations: [
    AppComponent,
    ProfileComponent,
    CollaborationComponent,
    OverviewComponent,
    RepositoriesComponent,
    StarsComponent,
    FollowersComponent,
    FollowingComponent,
    ProfileInfoComponent,
    SearchPipe
  ],
  imports: [
    BrowserModule, GithubRoutingModule, HttpModule, FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
