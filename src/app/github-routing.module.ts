import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from 'src/app/components/profile/profile.component';
import { OverviewComponent } from './components/collaboration/overview/overview.component';
import { RepositoriesComponent } from './components/collaboration/repositories/repositories.component';
import { StarsComponent } from './components/collaboration/stars/stars.component';
import { FollowersComponent } from './components/collaboration/followers/followers.component';
import { ProfileInfoComponent } from './components/profile/profile-info/profile-info.component';
import { FollowingComponent } from './components/collaboration/following/following.component';

const routes: Routes = [
  {
    path: '',
    component: ProfileComponent,
    children: [{
      path: '',
      redirectTo: 'repositories',
      pathMatch: 'full'
    },{
      path: 'overview',
      component: OverviewComponent
    }, {
      path: 'repositories',
      component: RepositoriesComponent
    }, {
      path: 'stars',
      component: StarsComponent
    }, {
      path: 'followres',
      component: FollowersComponent
    },
    {
      path: 'following',
      component: FollowingComponent
    }]
  },
  {
    path: '**',
    component: ProfileComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class GithubRoutingModule { }
