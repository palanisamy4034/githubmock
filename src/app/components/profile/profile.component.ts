import { Component, OnInit } from '@angular/core';
import { GithubServiceService } from '../../service/github-service.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  userinfo: any;
  constructor(private gitService: GithubServiceService) { }

  ngOnInit() {
    this.gitService.GetUserInfo().subscribe(data => {
      this.userinfo = data;
    });

  }

}
