import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-collaboration',
  templateUrl: './collaboration.component.html',
  styleUrls: ['./collaboration.component.css']
})
export class CollaborationComponent implements OnInit {
  @Input() userinfo: any;
  constructor() { }

  ngOnInit() {
  }

}
