import { Component, OnInit } from '@angular/core';
import { GithubServiceService } from '../../../service/github-service.service';
const langList = [{ id: 'css', name: 'CSS' },
{ id: 'JavaScript', name: 'JavaScript' },
{ id: 'TypeScript', name: 'TypeScript' },
{ id: 'Php', name: 'PHP' },
{ id: 'Python', name: 'Python' },
{ id: 'Java', name: 'Java' }
];
const repoTypeList = [{ id: 'public', name: 'public' }, { id: 'private', name: 'private' }, { id: 'sources', name: 'sources' },
{ id: 'fork', name: 'forkes' }, { id: 'archived', name: 'archived' }, { id: 'mirrors', name: 'mirrors' }];
@Component({
  selector: 'app-repositories',
  templateUrl: './repositories.component.html',
  styleUrls: ['./repositories.component.css']
})
export class RepositoriesComponent implements OnInit {
  searchText: any;
  repotype: any = 'All';
  repoTypeList = repoTypeList;
  language: any = 'All';
  repoList: any;
  langList = langList;
  constructor(private GService: GithubServiceService) { }

  ngOnInit() {
    this.GService.GetRepoDetails().subscribe(data => {
      this.repoList = data;
    });
  }

}
